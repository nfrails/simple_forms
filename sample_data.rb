# create categories
Category.create(name: 'Technology')
Category.create(name: 'Lifestyle')
Category.create(name: 'Business')
Category.create(name: 'Entertainment')
# create status
Status.create(name: 'just created')
Status.create(name: 'review')
Status.create(name: 'published')
# create tags
Tag.create(name: 'Fun')
Tag.create(name: 'Scary')
Tag.create(name: 'Sad')
Tag.create(name: 'Excited')