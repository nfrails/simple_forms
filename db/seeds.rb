# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# create categories
Category.create(name: 'Technology')
Category.create(name: 'Lifestyle')
Category.create(name: 'Business')
Category.create(name: 'Entertainment')
# create status
Status.create(name: 'just created')
Status.create(name: 'review')
Status.create(name: 'published')
# create tags
Tag.create(name: 'Fun')
Tag.create(name: 'Scary')
Tag.create(name: 'Sad')
Tag.create(name: 'Excited')
