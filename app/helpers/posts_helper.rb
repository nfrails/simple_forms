module PostsHelper
  def category_dropdown
    Category.all
  end

  def status_dropdown
    Status.all
  end
end
