class Post < ActiveRecord::Base
  belongs_to :category
  belongs_to :status
  has_many :taggings
  has_many :tags, through: :taggings

end
